<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserTask;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiUserTaskTest extends TestCase
{
    use DatabaseTransactions;

    public function user_auth_token() 
    {
        $user = User::find(1);
        $token = $user->createToken($user->id, ['only-user']);
        return $token->accessToken;
    }

    public function test_user_task_list()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->get('user-backsys/api/user-task/list');

        $response->assertStatus(200);
    }

    public function test_user_task_store()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->post('user-backsys/api/user-task/store', [
            'name' => 'Testing'
        ]);

        $response->assertStatus(200);
    }

    public function test_user_task_update()
    {
        $task = UserTask::find(1);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->put('user-backsys/api/user-task/update/' . $task->hash_id, [
            'name' => 'Testing'
        ]);

        $response->assertStatus(200);
    }

    public function test_user_task_complete()
    {
        $task = UserTask::find(1);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->put('user-backsys/api/user-task/complete/' . $task->hash_id);

        $response->assertStatus(200);
    }

    public function test_user_task_delete()
    {
        $task = UserTask::find(1);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->put('user-backsys/api/user-task/delete/' . $task->hash_id);

        $response->assertStatus(200);
    }
}
