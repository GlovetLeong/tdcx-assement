<?php

namespace Tests\Feature;

use App\Models\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiUserTest extends TestCase
{
    public function user_auth_token() 
    {
        $user = User::find(1);
        $token = $user->createToken($user->id, ['only-user']);
        return $token->accessToken;
    }

    public function test_login()
    {
        $response = $this->post('user-backsys/api/login', [
            'email' => env('TEST_USER_EMAIL'),
            'password' => env('TEST_USER_PASSWORD')
        ]);
        $response->assertStatus(200);
    }

    public function test_user_self()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->get('user-backsys/api/user/self');

        $response->assertStatus(200);
    }

    public function test_dashboard()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->get('user-backsys/api/dashboard/show');

        $response->assertStatus(200);
    }
}
