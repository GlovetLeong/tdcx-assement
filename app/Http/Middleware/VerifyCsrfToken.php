<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
		'user-backsys/api/*',
		'admin-backsys/api/*',
		'contributor-backsys/api/*',
		'reviewer-backsys/api/*'
    ];
}
