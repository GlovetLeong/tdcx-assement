<?php

namespace App\Http\Controllers\User\Api;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\User\UserModule;

use App\Http\Controllers\User\BaseController;

class UserController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function self(Request $request)
    {
    	$self = UserModule::self($request);
    	return $self;
    }
}
