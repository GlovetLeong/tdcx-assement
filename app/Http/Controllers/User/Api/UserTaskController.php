<?php

namespace App\Http\Controllers\User\API;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\User\UserTaskModule;

use App\Http\Controllers\User\BaseController;

class UserTaskController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list(Request $request)
    {
    	$user_task = UserTaskModule::list($request);
    	return $user_task;
    }

    public function show(Request $request, $hash_id)
    {
        $user_task = UserTaskModule::show($request, $hash_id);
        return $user_task;
    }

    public function store(Request $request)
    {
        $user_task = UserTaskModule::store($request);
        return $user_task;
    }

    public function update(Request $request, $hash_id)
    {
        $user_task = UserTaskModule::update($request, $hash_id);
        return $user_task;
    }

    public function complete(Request $request, $hash_id)
    {
        $user_task = UserTaskModule::complete($request, $hash_id);
        return $user_task;
    }

    public function delete(Request $request, $hash_id)
    {
        $user_task = UserTaskModule::delete($request, $hash_id);
        return $user_task;
    }
}
