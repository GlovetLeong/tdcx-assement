<?php

namespace App\Http\Controllers\User\Api;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\User\DashboardModule;

use App\Http\Controllers\User\BaseController;

class DashboardController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show(Request $request)
    {
    	$dashboard = DashboardModule::show($request);
    	return $dashboard;
    }
}
