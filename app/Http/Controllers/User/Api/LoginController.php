<?php

namespace App\Http\Controllers\User\Api;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\User\LoginModule;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
    	
    }

    public function login(Request $request)
    {
    	$login = LoginModule::login($request);
    	return $login;
    }
}
