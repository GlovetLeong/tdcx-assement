<?php

namespace App\Http\Controllers\User\Web;

use Auth;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class AppController extends Controller
{   
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        return view('user.app');
    }
}

