<?php

namespace App\Http\Controllers\User;
use Auth;
use App\Http\Controllers\Controller;

class BaseController extends Controller{
	
	public function __construct() {
		\Config::set('auth.guards.api.provider', 'users');
		$this->middleware(['auth:user-api', 'scope:only-user']);
	}
}
