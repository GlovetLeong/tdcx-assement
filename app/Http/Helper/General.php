<?php
namespace App\Http\Helpers;

use Illuminate\Support\Facades\Cache;

class General
{
    public function __construct()
    {

    }

    public static function storeCachePerviousQuery($query = '', $name = '')
    {   
        $query_string = urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', http_build_query($query)));
        Cache::put($name, $query_string, 1800);
    }

    public static function getCachePerviousQuery($name = '')
    {   
        return Cache::get($name);
    }

    public static function paginationGenerator($data = [], $params = [], $extract = [])
    {   
        $pagination = [];

        $data->appends($params)->links();
        $pagination = [
            'status' => true,
            'total' => $data->total(),
            'has_more_page' => $data->hasMorePages(),
            'query_string' => urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', http_build_query($params))),
            'page' => [
                'next_page' =>  urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', $data->nextPageUrl())),
                'pervious_page' =>  urldecode(preg_replace('/(%5B)\d+(%5D=)/i', '$1$2', $data->previousPageUrl())),
            ],
            'data' => $extract
        ];

        return $pagination;
    }
}

