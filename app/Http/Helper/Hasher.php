<?php
namespace App\Http\Helpers;

use Hashids\Hashids;

class Hasher
{
    public function __construct()
    {

    }

    public static function encode(...$args)
    {
        return app(Hashids::class)->encode(...$args);
    }

    public static function decode($enc)
    {
        if (is_int($enc)) {
            return $enc;
        }

        if(!empty(app(Hashids::class)->decode($enc))) {
            return app(Hashids::class)->decode($enc)[0];
        }
    }

    public static function decodeArray($set = [])
    {
        $return_set = [];
        foreach ($set as $key => $row) {

            $return_set[] = Hasher::decode($row);
        }

        return $return_set;
    }
}

