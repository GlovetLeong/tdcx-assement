<?php
namespace App\Http\Helpers;

use Illuminate\Support\Str;
use Image;

class ImageHandel
{
	function __construct()
	{

	}

	public static function getImgFullPath($img_info = [], $type = 'object')
	{
		$default_file =  asset('share/no-image.jpg');

		if ($type == 'object') {
			$default_file =  asset('share/no-image.jpg');
		}
		else if ($type == 'person') {
			$default_file =  asset('share/non-person.jpg');
		}
		else if ($type == 'cover') {
			$default_file =  asset('share/non-cover.jpg');
		}

		$img_info = json_decode($img_info);
		if (!empty($img_info)) {
			foreach ($img_info as $key_img => $row_img) {
				$img_info[$key_img]->file_info->original->file = asset($row_img->file_info->original->file);
				$img_info[$key_img]->file_info->large->file = asset($row_img->file_info->large->file);
				$img_info[$key_img]->file_info->medium->file = asset($row_img->file_info->medium->file);
				$img_info[$key_img]->file_info->small->file = asset($row_img->file_info->small->file);
			}
		}
		else {
			$img_info = [(object)[
				'empty' => true,
				'file_info' => (object)[
						'original' => (object) [
							'file' => $default_file
						],
						'large' => (object) [
							'file' => $default_file
						],
						'medium' => (object) [
							'file' => $default_file
						],
						'small' => (object) [
							'file' => $default_file
						],
				]
			]];
		}
	    return $img_info;
	}

	public static function extractFullPath($file = '')
	{
		$file = explode(asset('/'), $file);
		return $file[1];
	}

	public static function generateFolder($set_path = '')
    {
        $folder = public_path() . $set_path;
        \File::isDirectory($folder) or \File::makeDirectory($folder, 0777, true, true); //check folder is exist
        return $folder;
    }

    public static function safeUrlencode($txt)
    {
        $result = preg_replace_callback("/[^-\._~:\/\?#\\[\\]@!\$&'\(\)\*\+,;=]+/",
            function ($match) {
                return rawurlencode($match[0]);
            }, $txt);
        return ($result);
    }

    public static function insertImage($path = '', $file = '')
    {
        if (filter_var($file[0], FILTER_VALIDATE_URL) === false) {
            $image = ImageHandel::base64ToImage($path, $file);
        }
        else {
            $image = ImageHandel::urlToImage($path, $file);
        }
        return $image;
    }

    public static function urlToImage($path = '', $url_file = [])
    {
        $folder_path = ImageHandel::generateFolder('/share/images/' . $path);

    	foreach ($url_file as $key => $row) {
	        $filename = (string) Str::uuid();
	        $extension = explode('.', $row);
	        $extension = end($extension);
	        $filename_with_ext = $filename . '.' . $extension;

	        try {
	            $img = Image::make(ImageHandel::safeUrlencode($row))->save($folder_path . '/' . $filename_with_ext);
	        } catch (\Exception $e) {
	            \Log::info('invalid_image_url-' . $row);
	            return false;
	        }

	        $file = ImageHandel::generateImages($path, $filename_with_ext, $filename, $extension);

	        $file_info[] = [
	            'path' => $path,
	            'filename' => $filename_with_ext,
	            'mime_type' => $extension,
	            'extension' => $extension,
	            'file_info' => empty($file) ? [] : $file,
	        ];
    	}
        return $file_info;
    }


    public static function base64ToImage($path = '', $base64Set = [])
    {
    	$file_info = [];
        $folder_path = ImageHandel::generateFolder('/share/images/' . $path);

    	foreach ($base64Set as $key => $row) {

	    	$imgdata = $row;
	        $filename = (string) Str::uuid();

	        $f = finfo_open();
	        $mime_type = finfo_file($f, $imgdata, FILEINFO_MIME_TYPE);
	        
	        $extension = '';
	        switch ($mime_type) {
	            case 'image/gif':
	                $extension = 'gif';
	                break;
	            case 'image/png':
	                $extension = 'png';
	                break;
	            case 'image/jpeg':
	                $extension = 'jpg';
	                break;
	        }

	        if (empty($extension)) {
	            return false;
	        }

	        $filename_with_ext = $filename . '.' . $extension;
	        $img = Image::make($row)->save($folder_path . '/' . $filename_with_ext);

	        $file = ImageHandel::generateImages($path, $filename_with_ext, $filename, $extension);

	        $file_info[] = [
	            'path' => $path,
	            'filename' => $filename_with_ext,
	            'mime_type' => $mime_type,
	            'extension' => $extension,
	            'file_info' => empty($file) ? [] : $file,
	        ];
    	}
        return $file_info;
    }

    public static function generateImages($path = '', $file = '', $filename = '', $extension = '')
    {
        $file_info = [];
        
        $img = Image::make(public_path('share/images/' . $path) . '/' . $file);
        $file_info['original']['file'] = 'share/images/' . $path . '/' . $file;
        $file_info['original']['width'] = $img->width();
        $file_info['original']['height'] = $img->height();

        $img->widen(800, function ($constraint) {
            $constraint->upsize();
        })->heighten(800, function ($constraint) {
            $constraint->upsize();
        });
        $img->save(public_path('share/images/' . $path) . '/' . $filename . '_' . 'l.' . $extension);
        $file_info['large']['file'] = 'share/images/' . $path . '/' . $filename . '_' . 'l.' . $extension;
        $file_info['large']['width'] = $img->width();
        $file_info['large']['height'] = $img->height();

        $img->widen(480, function ($constraint) {
            $constraint->upsize();
        })->heighten(480, function ($constraint) {
            $constraint->upsize();
        });
        $img->save(public_path('share/images/' . $path) . '/' . $filename . '_' . 'm.' . $extension);
        $file_info['medium']['file'] = 'share/images/' . $path . '/' . $filename . '_' . 'm.' . $extension;
        $file_info['medium']['width'] = $img->width();
        $file_info['medium']['height'] = $img->height();

        $img->widen(320, function ($constraint) {
            $constraint->upsize();
        })->heighten(320, function ($constraint) {
            $constraint->upsize();
        });
        $img->save(public_path('share/images/' . $path) . '/' . $filename . '_' . 's.' . $extension);
        $file_info['small']['file'] = 'share/images/' . $path . '/' . $filename . '_' . 's.' . $extension;
        $file_info['small']['width'] = $img->width();
        $file_info['small']['height'] = $img->height();

        return $file_info;
    }

    public static function removeImage($img_info = [])
    {
        foreach ($img_info as $key_img => $row_img) {
        	if (!empty($row_img->path) && $row_img->path != 'seeder') {
	    		$img_original = ImageHandel::extractFullPath($row_img->file_info->original->file);
				$img_large = ImageHandel::extractFullPath($row_img->file_info->large->file);
				$img_medium = ImageHandel::extractFullPath($row_img->file_info->medium->file);
				$img_small = ImageHandel::extractFullPath($row_img->file_info->small->file);
	            \File::delete([$img_original, $img_large, $img_medium, $img_small]);
           }
        }
        return true;
    }
}