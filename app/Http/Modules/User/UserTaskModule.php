<?php

namespace App\Http\Modules\User;

use Auth;
use App\Models\UserTask;

use App\Http\Rules\User\UnderCheck;

use App\Http\Helpers\General;
use App\Http\Helpers\Hasher;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UserTaskModule
{
    public function __construct()
    {
        
    }

    public static function list(Request $request)
    {
        General::storeCachePerviousQuery($request->query(), 'user_task_query');

        $name = $request->get('name', null);
        $status = $request->get('status', []);
        $page = $request->get('page', null);
        $limit = $request->get('limit', 10);

        $query = UserTask::query();

        if (strlen($name) > 0) {
            $query->where('name', 'like', '%' . $name . '%');
        }

        if (!empty($status) > 0) {
            $query->whereIn('status', $status);
        }

        $user_id = Auth::guard('user-api')->user()->id;
        $query->where('user_id', $user_id);

        $query->whereNotIn('status', [2]);

        $query->orderBy('id', 'desc');

        $params = [
            'name' => $name,
            'status' => $status,
            'page' => $page
        ];

        $user_task_list = $query->paginate($limit);

        $extract = [];
        foreach ($user_task_list as $key => $row) {
            $extract[] = $row;
          
        }

        $pagination = General::PaginationGenerator($user_task_list, $params, $extract);

        return $pagination;
    }

    public static function show(Request $request, $hash_id)
    {
        $id = Hasher::decode($hash_id);
        $user_id = Auth::guard('user-api')->user()->id;

        $user_task = UserTask::
            where('id', $id)
            ->where('user_id', $user_id)
            ->first();
        if ($user_task) {

            $data = (object)[
                'status' => true,
                'data' => $user_task
            ];

            return response()->json($data);
        }
        return abort(404);
    }

    public static function store(Request $request)
    {
        $validation = UserTaskModule::validation($request);
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        DB::beginTransaction();

        $user_id = Auth::guard('user-api')->user()->id;
        $name = $request->input('name');

        $user_task = new UserTask();
        $user_task->user_id = $user_id;
        $user_task->name = $name;
        $user_task->status = 1;
        $user_task->save();

        DB::commit();

        $data = (object)[
            'status' => true,
            'data' => $user_task,
        ];

        return response()->json($data);
    }

    public static function update(Request $request, $hash_id)
    {
        $id = Hasher::decode($hash_id);
        $validation = UserTaskModule::validation($request, $id, 'PUT');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        DB::beginTransaction();

        $name = $request->input('name');

        $user_task = UserTask::find($id);
        $user_task->name = $name ?? $user_task->name;
        $user_task->update();

        DB::commit();

        $query_string = General::getCachePerviousQuery('user_task_query');
        $data = (object)[
            'status' => true,
            'pervious_query' => $query_string,
            'data' => $user_task
        ];

        return response()->json($data);
    }

    public static function complete(Request $request, $hash_id)
    {
        $id = Hasher::decode($hash_id);
        $validation = UserTaskModule::validation($request, $id, 'COMPLETE');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        DB::beginTransaction();

        $user_task = UserTask::find($id);
        $user_task->status = 3;
        $user_task->update();

        DB::commit();

        $query_string = General::getCachePerviousQuery('user_task_query');
        $data = (object)[
            'status' => true,
            'pervious_query' => $query_string,
            'data' => $user_task
        ];

        return response()->json($data);
    }

    public static function delete(Request $request, $hash_id)
    {
        $id = Hasher::decode($hash_id);
        $validation = UserTaskModule::validation($request, $id, 'DELETE');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        DB::beginTransaction();

        $user_task = UserTask::find($id);
        $user_task->status = 2;
        $user_task->update();

        DB::commit();

        $query_string = General::getCachePerviousQuery('user_task_query');
        $data = (object)[
            'status' => true,
            'pervious_query' => $query_string,
            'data' => $user_task
        ];

        return response()->json($data);
    }

    private static function validation(Request $request, $id = '', $method = 'POST')
    {
        $data = $request->all();
        $rule= [];

        if ($method == 'POST') {
            $rule = [
                'name' => 'required',
            ];
        }

        if ($method == 'PUT') {
            $data['id'] = $id;
            $rule = [
                'id' => [new UnderCheck('user_task')],
                'name' => 'required',
            ];
        }

        if ($method == 'COMPLETE') {
            $data['id'] = $id;
            $rule = [
                'id' => [new UnderCheck('user_task')],
            ];
        }

        if ($method == 'DELETE') {
            $data['id'] = $id;
            $rule = [
                'id' => [new UnderCheck('user_task')],
            ];
        }

        $validator = Validator::make($data, $rule, Config('error_code'));

        $errors = $validator->errors();

        if ($validator->fails()) {
            $data = (object)[
                'status' => false,
                'errors' => $errors
            ];
            return $data;
        }
        else {
            return (object)['status' => true];
        }
    }
}

