<?php

namespace App\Http\Modules\User;

use Auth;
use App\Models\UserTask;

use Illuminate\Http\Request;

class DashboardModule
{
    public function __construct()
    {

    }

    public static function show(Request $request)
    {
        $user_id = Auth::guard('user-api')->user()->id;

        $data = [
            'total_task' => UserTask::
                whereNotIn('status', [2])
                ->where('user_id', $user_id)
                ->count(),
            'total_uncompleted_task' => UserTask::
                where('status', 1)
                ->where('user_id', $user_id)
                ->count(),
            'total_completed_task' => UserTask::
                where('status', 3)
                ->where('user_id', $user_id)
                ->count(),
            'latest_task_list' => UserTask::
                whereNotIn('status', [2])
                ->where('user_id', $user_id)
                ->limit(3)->orderBy('id', 'desc')->get()
        ];

    	$data = (object)[
            'status' => true,
            'data' => $data
        ];

        return response()->json($data);
    }
}
