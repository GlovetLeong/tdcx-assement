<?php

namespace App\Http\Modules\User;

use Auth;
use Illuminate\Http\Request;

class UserModule
{
    public function __construct()
    {
        // parent::__construct();
    }

    public static function self(Request $request)
    {
    	$self = Auth::guard('user-api')->user();

    	$data = (object)[
            'status' => true,
            'data' => $self
        ];

        return response()->json($data);
    }
}
