<?php

namespace App\Http\Rules\User;
use Auth;

use App\Models\UserTask;

use App\Http\Helpers\Hasher;

use Illuminate\Contracts\Validation\Rule;

class UnderCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $type;
    private $should_not;

    public function __construct($type = '')
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $value = is_array($value) ? $value : [$value];

        $user_id = Auth::guard('user-api')->user()->id;

        if ($this->type == 'seller_shop') {
            foreach ($value as $key => $row) {
                $data = UserTask::
                    where('id', $row)
                    ->where('user_id', $user_id)
                    ->count();
                if (!$data) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'UNDER_ERROR';
    }
}
