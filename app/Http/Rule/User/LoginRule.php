<?php

namespace App\Http\Rules\User;

use Auth;
use App\Models\User;
use App\Models\UserSocialAccount;

use Illuminate\Contracts\Validation\Rule;
use OTPHP\TOTP;

class LoginRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    protected $type;
    protected $password;
    protected $totp;
    protected $pending_error;

    public function __construct(
        $type = '', 
        $password = '', 
        $totp = '', 
        $pending_error = false
    )
    {
        $this->type = $type;
        $this->password = $password;
        $this->totp = $totp;
        $this->pending_error = $pending_error;
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->type == 'manual') {
            $email = $value;
            $password = $this->password;

            if (Auth::guard('user')->attempt(['email' => $email, 'password' => $password])) {
                $data = User::
                        where('email', $email)
                        ->whereIn('status', [1, 9])
                        ->count();
                    if (!$data) {
                        return false;
                    }
                return true;
            }
        }

        // if ($this->type == 'social') {
        //     $social_media_type_id = $value;
        //     $provider_id = $this->password;
        //     $data = UserSocialAccount::
        //         where('social_media_type_id', $social_media_type_id)
        //         ->where('provider_id', $provider_id)
        //         ->whereHas('user', function($q) {
        //             $q->whereIn('status', [1]);
        //         })
        //         ->count();

        //     if (!$data) {
        //         return false;
        //     }
        //     return true;
        // }

        // if ($this->type == 'totp') {
        //     $social_media_type_id = $value;
        //     $provider_id = $this->password;
        //     $data = UserSocialAccount::
        //         where('social_media_type_id', $social_media_type_id)
        //         ->where('provider_id', $provider_id)
        //         ->count();
        //     if (!$data) {
        //         return false;
        //     }
        //     $totp = new TOTP(
        //         $social_media_type_id . $provider_id, 
        //         env('TOTP_SECRET'), 
        //         100,                 
        //         'sha512',           
        //         8                  
        //     );

        //     if (!$totp->verify($this->totp)) {
        //         return false;
        //     }
        //     return true;
        // }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (!$this->pending_error) {
            return config('error_code.login_error');
        }
        else {
            return config('error_code.pending_error');
        }
    }
}
