<?php

namespace App\Models;

use App\Http\Helpers\ImageHandel;

use Laravel\Passport\HasApiTokens;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guard_name = 'user';
    
    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password',
    ];

    public function getImgAttribute($value)
    {
        $file_info = [];
        $file_info = ImageHandel::getImgFullPath($value, 'person');
        return $file_info;
    }
}
