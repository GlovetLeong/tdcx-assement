<?php
	return [
		'login_error' => 'LOGIN_ERROR',
		'pending_error' => 'LOGIN_PENDING_ERROR',
		'required' => 'REQUIRED',
		'required_if' => 'REQUIRED',
		'email' => 'EMAIL_ERROR',
		'unique' => 'UNIQUE_ERRO',
		'confirmed' => 'CONFIRM_ERROR',
		'min' => [
	        'string' => 'MIN_ERROR_:min',
	    ],
	    'numeric' => 'NUMERIC_ERROR',
	    'gt' => [
	    	'numeric' => 'GREATER_ERROR_:value'
	    ],
	    'lt' => [
	    	'numeric' => 'LESS_ERROR_:value'
	    ],
	];