<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        // User
        $this->call(UsersTableSeeder::class);

        // User Task
        $this->call(UserTasksTableSeeder::class);

        DB::commit();
    }
}
