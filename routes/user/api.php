<?php

Route::group(['namespace' => 'User', 'prefix' => 'user-backsys/api'], function () {
	Route::group(['namespace' => 'Api'], function () {
    	Route::post('login', ['as' => 'api.user.login', 'uses' => 'LoginController@login']);
		// User
		Route::prefix('user')->group(function () {
			Route::get('self', 
	    		[
					'as' => 'api.user.self', 
					'uses' => 'UserController@self', 
	    		]
	    	);
		});

		Route::prefix('dashboard')->group(function () {
			Route::get('show', 
				[
					'as' => 'api.user.dashboard.list', 
					'uses' => 'DashboardController@show', 
	    		]
			);
		});

		// User Task
		Route::prefix('user-task')->group(function () {
			Route::get('list', 
	    		[
					'as' => 'api.user.user_task.list', 
					'uses' => 'UserTaskController@list', 
	    		]
	    	);
	    	Route::get('show/{hash_id}', 
	    		[
					'as' => 'api.user.user_task.show', 
					'uses' => 'UserTaskController@show', 
	    		]
	    	);
	    	Route::post('store', 
	    		[
					'as' => 'api.user.user_task.store', 
					'uses' => 'UserTaskController@store', 
	    		]
	    	);
	    	Route::put('update/{hash_id}', 
	    		[
					'as' => 'api.user.user_task.update', 
					'uses' => 'UserTaskController@update', 
	    		]
	    	);
	    	Route::put('complete/{hash_id}', 
	    		[
					'as' => 'api.user.user_task.complete', 
					'uses' => 'UserTaskController@complete', 
	    		]
	    	);
	    	Route::put('delete/{hash_id}', 
	    		[
					'as' => 'api.user.user_task.update', 
					'uses' => 'UserTaskController@delete', 
	    		]
	    	);
		});
	});
});