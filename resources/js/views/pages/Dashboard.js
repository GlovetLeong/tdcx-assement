import React, { useState } from 'react';

import TaskCard from '../../components/TaskCard';
import TaskList from '../../components/TaskList';

function Dashboard() {
    const [newKey, setNewKey] = useState(1);

    const refresh = () => {
        setNewKey(newKey + 1);
    }

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-12 mb-5">
                    <TaskCard
                        key={newKey}
                    />
                    <div className="mt-3">
                        <TaskList
                            key={newKey}
                            refresh={refresh}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Dashboard;