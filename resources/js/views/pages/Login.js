import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

function Login({refresh}) {

  const forceUpdate = React.useReducer(bool => !bool)[1];

  const history = useHistory();

  const [state , setState] = useState({
    form: new Form({
      email : '',
      password : ''
    })
  })

  const handleChange = (e) => {
    const {id , value} = e.target   
    setState(prevState => ({
      form: new Form({
      ...prevState.form,
        [id]:value
      })
    }));
  }

  const handleSubmit = e => {
    e.preventDefault();
    state.form.post(ENV.API_URL + 'login')
      .then(data => {
        sessionStorage.setItem('user_jwt', data.access_token);
        history.push('/dashboard');
        refresh();
      })
      .catch(function (error) {
        forceUpdate();
      });
  };

  return (
    <div className="container">
      <div className="col-sm-5 login-box">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">Login</h5>
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <input
                  type="email" 
                  id="email" 
                  className="form-control"  
                  placeholder="Email" 
                  value={state.form.email}
                  onChange={handleChange}
                />
                {
                  state.form.errors.has('email') ?
                    <div className="text-danger text-left">{ state.form.errors.get('email') }</div>
                 : 
                  ''
                }
              </div>
              <div className="form-group">
                <input
                  type="password" 
                  id="password" 
                  className="form-control"  
                  placeholder="Passsword" 
                  value={state.form.password}
                  onChange={handleChange}
                />
                {
                  state.form.errors.has('password') ?
                    <div className="text-danger text-left">{ state.form.errors.get('password') }</div>
                 : 
                  ''
                }
              </div>
              <button type="submit" className="btn btn-primary btn-block">Login</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;