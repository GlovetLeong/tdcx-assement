import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Navigation from './Navigation';
import Content from './Content';

function Index() {

	let host = window.location.hostname;
	let path = ENV.APP_URL.replace('http://' + host + '/', '');
	path = ENV.APP_URL.replace('https://' + host + '/', '');

    return (
		<BrowserRouter basename={path}>
			<Content/>
		</BrowserRouter>
    );
}

export default Index;
