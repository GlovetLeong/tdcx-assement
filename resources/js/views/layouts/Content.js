import React, { useState, useRef } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Login from '../pages/Login';
import Dashboard from '../pages/Dashboard';
import Navigation from './Navigation';

function useAuth() {
  return sessionStorage.getItem('user_jwt');
}

function PrivateRoute({ children, ...rest }) {
	let auth = useAuth();

	return (
		<Route
			{...rest}
			render={({ location }) =>
			auth ? (
				children
			) : (
			<Redirect
				to={{
					pathname: "/login",
					state: { from: location }
				}}
			/>
			)
			}
		/>
	);
}

function LoginRoute({ children, ...rest }) {
	let auth = useAuth();
	
	return (
		<Route
			{...rest}
			render={({ location }) =>
			!auth ? (
				children
			) : (
			<Redirect
				to={{
					pathname: "/dashboard",
					state: { from: location }
				}}
			/>
			)
			}
		/>
	);
}

function Content() {

	const navigationRef = useRef();

	const refresh = () => {
		navigationRef.current.refresh();
	}

	return (
		<div>
			<Navigation
				ref={navigationRef}
			/>
			<Switch>
				<LoginRoute path="/login">
	            	<Login 
	            		refresh={refresh}
	            	/>
	            </LoginRoute>
	            <PrivateRoute path="/">
	            	<Dashboard/>
	            </PrivateRoute>
	            <PrivateRoute path="/dashboard">
	            	<Dashboard/>
	            </PrivateRoute>
	        </Switch>
	    </div>
	)
}

export default Content;