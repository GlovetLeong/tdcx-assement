import React, { useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { useHistory } from 'react-router-dom';

function useAuth() {
  return sessionStorage.getItem('user_jwt');
}

const Navigation = forwardRef((props, ref) => {

    const history = useHistory();

    const [user, setUser] = useState(null);
    const [isAuth, setIsAuth] = useState(useAuth());

    useImperativeHandle(ref, () => ({
        refresh() {
            setIsAuth(useAuth());
        }
    }));

    const getUserDetail = () => {
        let url = 'user/self';
        General.getCall(url, {}, 0, false, sessionStorage.getItem('user_jwt')).then(data => {
           setUser(data.data);
        });
    }

    const userLogout = (e) => {
        e.preventDefault();
        sessionStorage.removeItem('user_jwt');
        history.push('/login');
        setIsAuth(useAuth());
    }

    useEffect(() => {
        if (!user && isAuth) {
            getUserDetail();
        }
    });

    return (
        <div>
        {
            isAuth ? 
                <nav className="navbar sticky-top navbar-light bg-white mb-5">
                    <a className="navbar-brand" href="#">
                        <div className="box">
                            <img src={user ? user.img[0].file_info.small.file : ''} className="rounded-circle" width="50" height="60" alt="" />
                            <span className="ml-1">{user ? user.username: ''} </span>
                        </div>
                    </a>
                    <ul className="navbar-nav justify-content-end">
                        <li className="nav-item">
                            <a className="nav-link" href="#" onClick={userLogout}>Logout</a>
                        </li>
                    </ul>
                </nav>
            :
            ''
        }
            
        </div>
    );
});

export default Navigation;