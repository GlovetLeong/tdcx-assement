import React, { useState, useEffect  } from 'react';
import SkeletonLoading from './SkeletonLoading';
import AddTaskModal from './AddTaskModal';
import EditTaskModal from './EditTaskModal';
import DeleteTaskModal from './DeleteTaskModal';
import CompleteTaskModal from './CompleteTaskModal';

function TaskList({refresh}) {

    const [hide, setHide] = useState(false);
    const [userTask, setUserTask] = useState(null);
    const [selectedTask, setSelectedTask] = useState(null);

    const getDashboard = () => {
        setTimeout(() => {
            let url = 'dashboard/show';
            General.getCall(url, {}, 0, false, sessionStorage.getItem('user_jwt')).then(data => {
                if(data.data.total_task === 0) {
                    setHide(true);
                }
            });
        }, 1000);
    }

    const getUserTask = (params = {}) => {
        setTimeout(() => {
            let url = 'user-task/list';
            General.getCall(url, params, 0, false, sessionStorage.getItem('user_jwt')).then(data => {
               setUserTask(data);
            });
        }, 1000);
    }

    const onSelectedTask = (task, type) => {
        setSelectedTask(task);
        if (type == 'edit') {
            $('#edit-task-modal').modal('toggle');
        }
        else if (type == 'delete') {
            $('#delete-task-modal').modal('toggle');
        }
        else if (type == 'complete') {
            $('#complete-task-modal').modal('toggle');
        }
    }

    const onSearch = e => {
        e.preventDefault();
       
        if (e.target.value.length > 3) {
            let params = {
                name: e.target.value
            }
            getUserTask(params);
        }

        if (e.target.value.length == 0) {
            getUserTask({});
        }
    };

    useEffect(() => {
        if (!userTask) {
            getDashboard();
            getUserTask();
        }
    });

    return (
        <div>
            {
                !hide ? 
                    <div className="row">
                        <div className="col-12">
                            <div className="row">
                                <div className="col-3">
                                    <h5 className="card-title">Tasks</h5>
                                </div>
                                <div className="col-9">
                                    <div className="d-flex">
                                        <div className="input-group mr-2">
                                            <input type="text" className="form-control" 
                                                onChange={onSearch}
                                            />
                                            <div className="input-group-append">
                                                <span className="input-group-text">
                                                    <i className="fa fa-search"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <button
                                            className="btn btn-xs btn-primary" 
                                            data-toggle="modal" data-target="#add-task-modal"
                                        >
                                            <i className="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mt-3">
                            <div className="card">
                                <div className="card-body">
                                    {
                                        userTask ? 
                                        <table className="table table-striped mb-0">
                                            <tbody>
                                                {
                                                    (userTask.data[0]) ?
                                                        userTask.data.map((task, index) =>
                                                            <tr className="" key={index}>
                                                                <td>
                                                                    <span className={ task.status === 3 ? 'text-decoration-line' : ''}>
                                                                        {task.name}
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <div className="text-right">
                                                                        {
                                                                             task.status !== 3 ?
                                                                                <button
                                                                                    className="btn btn-xs btn-light mr-2"
                                                                                    onClick={() => onSelectedTask(task, 'complete')}
                                                                                >
                                                                                    <i className="fa fa-check"></i>
                                                                                </button>
                                                                             :

                                                                             ''
                                                                        }

                                                                        <button
                                                                            className="btn btn-xs btn-light mr-2"
                                                                            onClick={() => onSelectedTask(task, 'edit')}
                                                                        >
                                                                            <i className="fa fa-pen"></i>
                                                                        </button>
                                                                        <button
                                                                            className="btn btn-xs btn-light" 
                                                                            onClick={() => onSelectedTask(task, 'delete')}
                                                                        >
                                                                            <i className="fa fa-trash-alt"></i>
                                                                        </button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    :
                                                    <tr>
                                                        <td colSpan="2" className="text-center">No Result Found</td>
                                                    </tr>
                                                }
                                                
                                            </tbody>
                                        </table>
                                        :
                                        <SkeletonLoading 
                                            num={8}
                                        />
                                    }
                                </div>
                            </div>
                        </div>
                       
                        <EditTaskModal 
                            refresh={refresh}
                            task={selectedTask}
                        />
                        <DeleteTaskModal 
                            refresh={refresh}
                            task={selectedTask}
                        />
                        <CompleteTaskModal 
                            refresh={refresh}
                            task={selectedTask}
                        />
                    </div>
            :
                <div>

                    <div className="col-sm-5 first-task-box">
                        <div className="card task-card">
                            <div className="card-body text-center p-5">
                                <h5 className="card-title">You Have No Task</h5>
                                <div>
                                    <button
                                        className="btn btn-xs btn-primary" 
                                        data-toggle="modal" data-target="#add-task-modal"
                                    >
                                        <i className="fa fa-plus"></i> New Task
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            <AddTaskModal 
                refresh={refresh}
            />
        </div>
    );
}

export default TaskList;