import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

function SkeletonLoading({num = 8}) {
    return (
        <div>
			<SkeletonTheme color="#f1f1f1" highlightColor="#fff">
				<Skeleton count={num} />
			</SkeletonTheme>
        </div>
    );
}

export default SkeletonLoading;