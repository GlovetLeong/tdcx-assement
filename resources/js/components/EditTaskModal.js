import React, { useState, useEffect } from 'react';

function EditTaskModal({refresh, task}) {

    const [state , setState] = useState({
        form: new Form({
            name : ''
        })
    })

    const handleChange = (e) => {
        const {id , value} = e.target   
        setState(prevState => ({
            form: new Form({
                ...prevState.form,
                [id]:value
            })
        }));
    }

    const handleSubmit = e => {
        e.preventDefault();

        state.form.put(
            ENV.API_URL + 'user-task/update/' + task.hash_id, {
            headers: {
                'Authorization': 'Bearer '+ sessionStorage.getItem('user_jwt')
            }
        })
        .then(data => {
            $('#edit-task-modal').modal('toggle');
            refresh();
        })
        .catch(function (error) {
            forceUpdate();
        });
    };

    useEffect(() => {
        if (task) {
            setState(prevState => ({
                form: new Form({
                    ...prevState.form,
                    name: task.name
                })
            }));
        }
    }, [task]);

    return (
        <div className="modal fade" id="edit-task-modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                            <i className="fa fa-pen"></i> Edit Task</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className="modal-body">
                            <input
                                type="text" 
                                id="name" 
                                className="form-control"  
                                placeholder="Task Name" 
                                value={state.form.name}
                                onChange={handleChange}
                            />
                            {
                                state.form.errors.has('name') ?
                                    <div className="text-danger text-left">{ state.form.errors.get('name') }</div>
                                : 
                                    ''
                            }
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default EditTaskModal;