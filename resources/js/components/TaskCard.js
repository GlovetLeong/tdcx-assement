import React, { useState, useEffect  } from 'react';

import { PieChart } from 'react-minimal-pie-chart';
import SkeletonLoading from './SkeletonLoading';

function TaskCard() {

    const [hide, setHide] = useState(false);
    const [dashboard, setDashboard] = useState(null);
    const [dataPie, setDataPie] = useState([]);

    const getDashboard = () => {
        setTimeout(() => {
            let url = 'dashboard/show';
            General.getCall(url, {}, 0, false, sessionStorage.getItem('user_jwt')).then(data => {
               setDashboard(data.data);

                if (data.data.total_uncompleted_task > 0) {
                    setDataPie(dataPie => dataPie.concat(
                        {
                            color: "#ccc",
                            value: data.data.total_uncompleted_task
                        }
                    ))
                }

                if (data.data.total_completed_task > 0) {
                    setDataPie(dataPie => dataPie.concat(
                        {
                            color: "#007bff",
                            title: "Completed Tasks",
                            value: data.data.total_completed_task
                        }
                    ))
                }

                if(data.data.total_task === 0) {
                    setHide(true);
                }
            });
        }, 1000);
    }

    useEffect(() => {
        if (!dashboard) {
            getDashboard();
        }
    });

    return (
        <div>
            {
                !hide ? 
                    <div className="row">
                        <div className="col-sm-4 mb-3">
                            <div className="card task-card">
                                <div className="card-body">
                                    {
                                            dashboard ?
                                                <div>
                                                    <h5 className="card-title">Tasks Completed</h5>
                                                    <span className="big-complete-task-number">{ dashboard.total_completed_task }</span> / { dashboard.total_task }
                                                </div>
                                            :
                                                <SkeletonLoading 
                                                num={5}
                                                />

                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 mb-3">
                            <div className="card task-card">
                                <div className="card-body">
                                    {
                                        dashboard ?
                                            <div>
                                                <h5 className="card-title">Latest Created Tasks</h5>
                                                <ul className="list-group latest-task-card">
                                                    {
                                                        dashboard.latest_task_list.map((task, index) =>
                                                            <li className="" key={index}>
                                                                <span className={ task.status === 3 ? 'text-decoration-line' : ''}>
                                                                    {task.name}
                                                                </span>
                                                            </li>
                                                        )
                                                    }
                                                </ul>
                                            </div>
                                        :
                                            <SkeletonLoading 
                                                num={5}
                                            />
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 mb-3">
                            <div className="card task-card">
                                <div className="card-body">
                                    {
                                        dashboard ?
                                            <div>
                                                <PieChart
                                                    data={dataPie}
                                                    labelPosition={65}
                                                    label={(dataPie) => dataPie.dataEntry.title}
                                                    labelStyle={{
                                                        fontSize: "10px",
                                                        fontColor: "FFFFFF",
                                                        fontWeight: "800",
                                                    }}
                                                />
                                            </div>
                                        :
                                            <SkeletonLoading 
                                                num={5}
                                            />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                :
                ''
            }
        </div>
    );
}

export default TaskCard;