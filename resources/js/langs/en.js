const En = {
	// Input
	'name': 'name',
	'email': 'email',
	'password': 'password',

	// Error 
	'incorrect_account_or_password': 'Incorrect account or password',
	'this_field_required': 'This %(att)s field required',
	'incorrect_email_format': 'Incorrect email format',
};

export default En;