
module.exports.getCall = function(url, params = {}, recall = 0, replace = false, token) {
	if (recall <= 3) {
	return axios.get(
		ENV.API_URL + url, {
			params, 
			headers: {
				'Authorization': 'Bearer '+ token
			}
		}).then(response => {
			return response.data
		})
		.catch(function (error) {
			if (recall >= 3 && error.response.status == 401) {
				module.exports.logout();
			}
			console.log(recall++);
			return module.exports.getCall(url, params, recall++);
		});
	}
};

module.exports.lang = function(code) {
	let lang_selected = localStorage.getItem('lang_selected');
	if (lang_selected == 'cn') {
		return Cn[code];
	}
	else {
		return En[code];
	}
};

module.exports.logout = function() {
	sessionStorage.removeItem('user_jwt');
    window.location.replace(ENV.APP_URL + 'login');
};

module.exports.errorData = function(data) {
	let error_data = [];
	let error_message = '';
 	Object.keys(data).forEach(function(key) {
		error_data.push(data[key]);
 	});
 	error_message = error_data.join('<br>');
 	return error_message;
};