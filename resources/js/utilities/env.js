const ENV = {
   'APP_URL': process.env.MIX_APP_URL,
   'API_URL': process.env.MIX_API_URL + 'user-backsys/api/',
   'SHARE_URL': process.env.MIX_API_URL + 'share/'
};
export default ENV;