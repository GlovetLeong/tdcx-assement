import Errors from './errors';

class Form {
   
    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();
    }

    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }

    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }

        this.errors.clear();
    }

    post(url, header) {
        return this.submit('post', url, header);
    }
  
    put(url, header) {
        return this.submit('put', url, header);
    }

    patch(url, header) {
        return this.submit('patch', url, header);
    }

    delete(url, header) {
        return this.submit('delete', url, header);
    }

    submit(requestType, url, header) {
        let vm = this;
        return new Promise((resolve, reject) => {
            axios[requestType](url, this.data(), header)
                .then(response => {
                    this.onSuccess(response.data);
                    resolve(response.data);
                })
                .catch(error => {

                    let error_return = error.response.data.errors;
                    Object.keys(error_return).forEach(function (key) {
                        error.response.data.errors[key] = vm.errorCodeConvert(error_return[key], key);
                    });

                    this.onFail(error.response.data.errors)
                    return reject(error.response.data.errors)
                });
        });
    }

    onSuccess(data) {
        // this.reset();
    }

    onFail(errors) {
        this.errors.record(errors);
    }

    errorCodeConvert(code = [], key) {
        let value = {
            att: General.lang(key)
        }
        let return_message = [];
        for (var i = 0; i < code.length; i++) {
            if(code[i] == 'LOGIN_ERROR') {
                return_message.push(General.lang('incorrect_account_or_password'));                
            }
            else if (code[i] == 'REQUIRED') {
                return_message.push(Sprintf(General.lang('this_field_required'), value));                
            }
            else if(code[i] == 'EMAIL_ERROR') {
                return_message.push(General.lang('incorrect_email_format'));                
            }
            else if(code[i] == 'UNIQUE_ERRO') {
                return_message.push(Sprintf(General.lang('the_content_has_already_been_taken'), value));                
            }
            else if(code[i] == 'CONFIRM_ERROR') {
                return_message.push(Sprintf(General.lang('the_content_confirmation_does_not_match'), value));                
            }
        }
        return return_message;
    }
}

export default Form; 